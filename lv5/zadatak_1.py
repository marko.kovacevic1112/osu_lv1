import numpy as np
import matplotlib
import matplotlib.pyplot as plt

from sklearn.datasets import make_classification
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import (
    confusion_matrix,
    ConfusionMatrixDisplay,
    accuracy_score,
    recall_score,
    precision_score
)
#parametri modela
# get the intercept and coefficients
# intercept = model.intercept_
# coefficients = model.coef_





X, y = make_classification(n_samples=200, n_features=2, n_redundant=0, n_informative=2,
                            random_state=213, n_clusters_per_class=1, class_sep=1)

# train test split
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=5)

plt.scatter(X_train[:,0], X_train[:,1], c=y_train, label='Podatci za učenje')
plt.scatter(X_test[:,0], X_test[:,1], c=y_test, marker='x', label='podatci za testiranje')
plt.legend()
plt.xlabel('x1')
plt.ylabel('x2')
plt.show()

#kreiranje i treniranje(ucenje) modela
logisticRegression = LogisticRegression()
logisticRegression.fit(X_train, y_train)

print(logisticRegression.coef_)
print(logisticRegression.intercept_)

intercept = logisticRegression.intercept_[0]
coef_1 = logisticRegression.coef_[0][0]
coef_2 = logisticRegression.coef_[0][1]

#compute the decision boundary curve - granica odluke(pravac)
x1 = np.linspace(np.min(X_train[:, 0]), np.max(X_train[:, 0]), 100)
x2 = (-intercept - coef_1*x1) / coef_2

plt.plot(x1,x2, c='r')
plt.scatter(X_train[:,0], X_train[:,1], c=y_train, label='Podatci za učenje')
plt.scatter(X_test[:,0], X_test[:,1], c=y_test, marker='x', label='podatci za testiranje')
plt.legend()
plt.xlabel('x1')
plt.ylabel('x2')
plt.show()

#Provedite klasifikaciju skupa podataka za testiranje pomocu izgradenog modela logisticke regresije
y_test_predicted = logisticRegression.predict(X_test)

# #Izracunajte i prikažite matricu zabune na testnim podacima.
confusionMatrix = confusion_matrix(y_test, y_test_predicted)
disp = ConfusionMatrixDisplay(confusion_matrix(y_test, y_test_predicted))
disp.plot()
plt.show()
#Izracunajte tocnost, preciznost i odziv na skupu podataka za testiranje
print(f"Tocnost: {accuracy_score(y_test, y_test_predicted)}")
print(f"Preciznost: {precision_score(y_test, y_test_predicted)}")
print(f"Odziv: {recall_score(y_test, y_test_predicted)}")

plt.scatter(X_test[:,0],X_test[:,1],c=y_test_predicted)
correct_mask = y_test_predicted == y_test
plt.scatter(X_test[correct_mask, 0], X_test[correct_mask, 1], c='green', label='Točno')

# plot the misclassified test data points in black
incorrect_mask = y_test_predicted != y_test
plt.scatter(X_test[incorrect_mask, 0], X_test[incorrect_mask, 1], c='black', label='Netočno')
plt.legend()
plt.show()