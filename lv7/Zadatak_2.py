import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as Image
from sklearn.cluster import KMeans
from sklearn.utils import shuffle
from sklearn.metrics import pairwise_distances_argmin
import os

# ucitaj sliku
img = Image.imread("imgs\\test_1.jpg")

# prikazi originalnu sliku
# plt.figure()
# plt.title("Originalna slika")
# plt.imshow(img)
# plt.tight_layout()
# plt.show()

# pretvori vrijednosti elemenata slike u raspon 0 do 1
img = img.astype(np.float64) / 255

# transfromiraj sliku u 2D numpy polje (jedan red su RGB komponente elementa slike)
w,h,d = img.shape
img_array = np.reshape(img, (w*h, d))

# rezultatna slika
img_array_aprox = img_array.copy()

n_colors = 5    

print("Broj razlicitih boja: ", len(np.unique(img_array, axis=0)))

# The shuffle function is used to randomly shuffle the input data before using it to fit the K-Means model or to predict color indices 
# on the full image. This is done to ensure that the training data is representative of the overall distribution of colors in the image 
# and to prevent any bias that may be present in the data. By shuffling the data, we make sure that each sample has an equal chance of being
# selected, regardless of its position in the original data array. This can help to improve the accuracy and generalization of the K-Means model.
img_array_sample = shuffle(img_array, random_state=0, n_samples=1_000)
kmeans = KMeans(n_clusters=n_colors, n_init="auto", random_state=0).fit(img_array_sample)
labels = kmeans.predict(img_array)

#Vrijednost svakog elementa slike originalne slike zamijeni s njemu pripadajucim centrom
def recreate_image(codebook, labels, w, h):
    #Recreate the (compressed) image from the code book & labels
    return codebook[labels].reshape(w, h, -1)


# Display all results, alongside original image
plt.figure(1)
plt.clf()
plt.axis("off")
plt.title("Original image (96,615 colors)")
plt.imshow(img)

plt.figure(2)
plt.clf()
plt.axis("off")
plt.title(f"Quantized image ({n_colors} colors, K-Means)")
plt.imshow(recreate_image(kmeans.cluster_centers_, labels, w, h))

plt.show()


#ucitaj sve slike
directory = "imgs\\"
images = []
for filename in os.listdir(directory):
    if filename.endswith(".jpg"):
        img_path = os.path.join(directory, filename)
        img = Image.imread(img_path)
        images.append(img)

i = 4
for img in images:
    img = img.astype(np.float64) / 255
    w,h,d = img.shape
    img_array = np.reshape(img, (w*h, d))
    img_array_aprox = img_array.copy()
    img_array_sample = shuffle(img_array, random_state=0, n_samples=1_000)
    kmeans = KMeans(n_clusters=n_colors, n_init="auto", random_state=0).fit(img_array_sample)
    labels = kmeans.predict(img_array)
    #show original img
    plt.figure(i)
    plt.clf()
    plt.axis("off")
    plt.title("Original image (96,615 colors)")
    plt.imshow(img)
    #show quantified img
    plt.figure(i+1)
    plt.clf()
    plt.axis("off")
    plt.title(f"Quantized image ({n_colors} colors, K-Means)")
    plt.imshow(recreate_image(kmeans.cluster_centers_, labels, w, h))
    i+=2

# plt.show()

#lakat metoda (elbow method)
#K je broj klustera a J je suma kvadriranih udaljenosti uzoraka od njihovog najblizeg klustera (inertia) => sto je inertia manja to bolje
plt.figure()
for i in range(1, 9):
    km = KMeans(n_clusters=i, init="random", n_init=5, random_state=0)
    km.fit(img_array_aprox)
    plt.plot(i, km.inertia_, ".-r", linewidth=2)
    plt.xlabel("K")
    plt.ylabel("J")

plt.show()



#random kvantiziranje slike

# codebook_random = shuffle(img_array, random_state=0, n_samples=n_colors)
# print("Predicting color indices on the full image (random)")
# labels_random = pairwise_distances_argmin(codebook_random, img_array, axis=0)

# plt.figure(3)
# plt.clf()
# plt.axis("off")
# plt.title(f"Quantized image ({n_colors} colors, Random)")
# plt.imshow(recreate_image(codebook_random, labels_random, w, h))