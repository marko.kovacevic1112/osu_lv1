import numpy as np
import matplotlib.pyplot as plt

# data = np.read_csv("data.csv")
data = np.genfromtxt("data.csv",delimiter=",",skip_header=1)

#a)
print(f"mjerenja su izvrsena na {len(data)} osoba")

#b)

height = data[:,1]
weight = data[:,2]
print(weight)

plt.scatter(height,weight)
plt.xlabel("height")
plt.ylabel("weight")
plt.title("height and weight distribution")
plt.show()

#c)
#every nth element
#first element::size of the step => in this case: first element and then every 50th
every50th = data[0::50]
height50 = every50th[:,1]
weight50 = every50th[:,2]

plt.scatter(height50,weight50)
plt.xlabel("height")
plt.ylabel("weight")
plt.title("height and weight distribution")
plt.show()

#d) min, max, srednja vrijednost visine
# print(data[:,1])
print(f"min: {data[:,1].min()}") #min
print(f"max: {data[:,1].max()}") #max
print(f"average: {data[:,1].mean()}") #average

#e)  min, max, srednja vrijednost visine (samo za muskarce i samo za zene)
#boolean indeksiranje

ind = data[:,0] == 1 #muskarci
men = data[ind]

print(f"min: {men[:,1].min()}") #min
print(f"max: {men[:,1].max()}") #max
print(f"average: {men[:,1].mean()}") #average

ind = data[:,0] == 0
woman = data[ind]

print(f"min: {woman[:,1].min()}") #min
print(f"max: {woman[:,1].max()}") #max
print(f"average: {woman[:,1].mean()}") #average