import numpy as np
import matplotlib.pyplot as plt

# x = np.array([1,2,2,3,3,4,4,3,3,2,2,1,1])
# y = np.array([3,3,4,4,3,3,2,2,1,1,2,2,3])

# plt.plot(x,y, marker="o")
# plt.show()
x = np.array([2, 3, 3, 1, 2])
y = np.array([2, 2, 1, 1, 2])


plt.plot(x,y, 'ro', color='blue')
plt.plot(x,y)
plt.axis([0, 4, 0, 4])
plt.show()
