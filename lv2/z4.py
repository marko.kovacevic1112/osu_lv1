import numpy as np
import matplotlib.pyplot as plt

# Napišite program koji ce kreirati sliku koja sadrži ´ cetiri kvadrata crne odnosno ˇ
# bijele boje (vidi primjer slike 2.4 ispod). Za kreiranje ove funkcije koristite numpy funkcije
# zeros i ones kako biste kreirali crna i bijela polja dimenzija 50x50 piksela. Kako biste ih složili
# u odgovarajuci oblik koristite numpy funkcije ´ hstack i vstack.
black = np.ones((50,50))
white = np.zeros((50,50))

whiteBlackColumn = np.vstack((black,white))
blackWhiteColumn = np.vstack((white,black))

img = np.hstack((blackWhiteColumn, whiteBlackColumn))

plt.imshow(img, cmap='gray')
plt.axis([0,100,0,100])
plt.show()
