import numpy as np
import matplotlib.pyplot as plt
from PIL import Image
from PIL import ImageEnhance

#prvi nacin:--------------------------
# img = plt.imread("road.jpg")
img = Image.open("road.jpg")

# #slika prije posvjetljivanja
plt.figure()
plt.imshow(img)
plt.show()

img = np.asarray(img)

# #ako imam na primjer broj 220 + 50 = 270 ali ce to zapravo biti 270 - 255 = 15
# #iz tog razloga pamtim sve brojeve koji ce se "preliti" preko 255 u mask varijabli (njihove indexe pamtim) i postavljam ih na 255
# #jer ako se preliju preko 255 postat ce tamni (0 je najtamniji a 255 najsvjetliji)
# mask = img > 205

img = img + 50 #make it brighter

img[mask] = 255
#slika nakon posvjetljivanja
plt.figure()
plt.imshow(img)
plt.show()

# # #drugi nacin:--------------------------
# # #using PIL
# # # Load image
img = Image.open("road.jpg")
# Increase brightness
enhancer = ImageEnhance.Brightness(img)
brighter_img = enhancer.enhance(1.6)

# plt.figure()
# plt.imshow(img)

# plt.figure()
# plt.imshow(brighter_img)
# plt.show()


#b)
img = np.asarray(img)
print(img.shape)

quarter = img[107:213,0:640]

plt.figure()
plt.imshow(img)

plt.figure()
plt.imshow(quarter)
plt.show()

#c)

rotatededImg = np.rot90(img)
rotatededImg = np.rot90(rotatededImg)
rotatededImg = np.rot90(rotatededImg)
plt.imshow(rotatededImg)
plt.show()

#d)

flipedImage = np.flipud(img)
plt.imshow(flipedImage)
plt.show()