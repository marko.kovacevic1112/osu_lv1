import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
#  .info() – daje osnovne informacije o DataFrameu
#  .head(n) – vraca prvih n zapisa u DataFrameu
#  .tail(n) - vraca zadnjih n zapisa u DataFrameu
# • .describe() – vraca statistiku za svaku velicinu u DataFrameu
# • .mean() – srednja vrijednost svakog stupca (velicine)
# • .median() – medijan vrijednost svakog stupca (velicine)
# • .max() - maksimalna vrijednost svakog stupca (velicine)
# • .min() – minimalna vrijednost svakog stupca (velicine)
# • .sort_values(by=['col']) – sortiranje DataFrame prema željenom stupcu col
# .iloc - izdvajanje redaka
# .isnull() - izostale vrijednosti po svakom stupcu
data = pd.read_csv("data_C02_emission.csv")

print(data['Cylinders'])
print ( data . Cylinders > 2 )
print ( data [ data . Cylinders > 6])
print(data[(data['Cylinders'] == 4) & ( data ['Engine Size (L)'] > 2.4 )].Model)

grouped = data . groupby ( 'Cylinders')
grouped . boxplot ( column = ['CO2 Emissions (g/km)'])
data . boxplot ( column =['CO2 Emissions (g/km)'], by='Cylinders')
plt . show ()


data = {'country': ['Italy','Spain','Greece','France','Portugal'],
'population': [59 , 47 , 11 , 68 , 10 ],
'code': [39 , 34 , 30 , 33 , 351 ] }
countries = pd . DataFrame ( data , columns = ['country', 'population', 'code']
)


#query example:
a = countries.query('country == "Spain"')['code'][1]
