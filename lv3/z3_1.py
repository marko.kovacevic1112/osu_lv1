import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

data = pd.read_csv("data_C02_emission.csv")

# #a)
print(f"dataframe sadrzi {len(data)} mjerenja")
print(data.dtypes)
print({data.isnull().sum()})
print(f"There are {data.duplicated().sum()} duplicates")

# #konvertiranje u tip category
# #kategoricke velicine:Make,Model,Vehicle Class, Transmission, Fuel Type
data['Make'] = pd.Categorical(data.Make)
data['Model'] = pd.Categorical(data.Model)
data['Vehicle Class'] = pd.Categorical(data['Vehicle Class'])
data['Transmission'] = pd.Categorical(data['Transmission'])
data['Fuel Type'] = pd.Categorical(data['Fuel Type'])
print(data.dtypes)

# # b) Tri automobila s najevcom i najmanjom gradskom potrosnjom, prikazati samo odredjene stupce(Make,Model,Fuel COnsumption City)
biggestCityConsumption3 = data.sort_values(by=['Fuel Consumption City (L/100km)'], ascending=False).head(3)
leastCityConsumption3 = data.sort_values(by=['Fuel Consumption City (L/100km)'], ascending=True).head(3)
print(biggestCityConsumption3[["Make", "Model", "Fuel Consumption City (L/100km)"]])
print(leastCityConsumption3[["Make", "Model", "Fuel Consumption City (L/100km)"]])

#  c) Koliko vozila ima velicinu motora izme ˇ du 2.5 i 3.5 L? Kolika je prosje ¯ cna C02 emisija ˇ
# plinova za ova vozila?

print(data.query("'Engine Size (L)' >= 2.5 and 'Engine Size (L)' < 3.5"))
print(len(data.query("`Engine Size (L)` >= 2.5 and `Engine Size (L)` <= 3.5")))
print(data.query("`Engine Size (L)` >= 2.5 and `Engine Size (L)` <= 3.5")['CO2 Emissions (g/km)'].mean())

#d)Koliko mjerenja se odnosi na vozila proizvoda¯ ca Audi? Kolika je prosje ˇ cna emisija C02 ˇ
#plinova automobila proizvoda¯ ca Audi koji imaju 4 cilindara?
print(len(data.query('Make == "Audi"')))
print(data.query('Make == "Audi" and Cylinders == 4')['CO2 Emissions (g/km)'].mean())

#e)Koliko je vozila s 4,6,8. . . cilindara? Kolika je prosjecna emisija C02 plinova s obzirom na broj cilindara?
print(len(data.query('Cylinders > 3 and Cylinders % 2 == 0')))
cylinders = data.query('Cylinders > 3 and Cylinders % 2 == 0').groupby(by='Cylinders')
print(cylinders['CO2 Emissions (g/km)'].mean())
#broj redaka po svakoj grupi
print(cylinders.size())

#f)Kolika je prosjecna gradska potrošnja u slu ˇ caju vozila koja koriste dizel, a kolika za vozila ˇ
#koja koriste regularni benzin? Koliko iznose medijalne vrijednosti?

#backtick se treba koristiti za stupce koji imaju razmak u imenu
print(data.query('`Fuel Type` == "Z"')['Fuel Consumption City (L/100km)'].mean())
print(data.query('`Fuel Type` == "Z"')['Fuel Consumption City (L/100km)'].median())
print(data.query('`Fuel Type` == "D"')['Fuel Consumption City (L/100km)'].mean())
print(data.query('`Fuel Type` == "D"')['Fuel Consumption City (L/100km)'].median())
print(type(data['Fuel Type'][0]))

#g) Koje vozilo s 4 cilindra koje koristi dizelski motor ima najvecu gradsku potrošnju goriva? ´

print(data.query("Cylinders == 4 and `Fuel Type` == 'D'")['Fuel Consumption City (L/100km)'].max())

#h) Koliko ima vozila ima rucni tip mjenja ˇ ca (bez obzira na broj brzina)? ˇ
print(len(data.query('Transmission.str.startswith("M")')))

# i) Izracunajte korelaciju izmedju numerickih velicina.
print(data.corr(numeric_only=True))