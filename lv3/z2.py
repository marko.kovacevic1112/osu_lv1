import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

#vodic
#stupcasti dijagram -> data.plot(kind='bar')
#histogram -> data.hist(bins = 40)
#kutijasti dijagram -> data.boxplot(column=['column_name'],by='')
#dijagram rasprsenja -> scatterplot

data = pd.read_csv("data_C02_emission.csv")
#a) Pomocu histograma prikažite emisiju C02 plinova. Komentirajte dobiveni prikaz.

plt.hist(data['CO2 Emissions (g/km)'], bins=40)
plt.show()

#b)Pomocu dijagrama raspršenja prikažite odnos izmedu gradske potrošnje goriva i emisije ¯
# C02 plinova. Komentirajte dobiveni prikaz. Kako biste bolje razumjeli odnose izmedu¯
# velicina, obojite to ˇ ckice na dijagramu raspršenja s obzirom na tip goriva.

colors = {'Z': 'red', 'X': 'blue', 'D': 'green', 'E': 'yellow'}
plt.scatter(data['Fuel Consumption City (L/100km)'],data['CO2 Emissions (g/km)'],c=data['Fuel Type'].map(colors))
plt.xlabel('potrosnja goriva')
plt.ylabel('co2 emisija')
plt.show()
print(colors)

# c) Pomocu kutijastog dijagrama prikažite razdiobu izvangradske potrošnje s obzirom na tip ´
# goriva. Primjecujete li grubu mjernu pogrešku u podacima?
#nacin 1: pomocu groupby

fuelType = data.groupby(by='Fuel Type')
fuelType.boxplot(column=['Fuel Consumption Hwy (L/100km)'])

# #nacin 2: pomocu by parametra iz boxplota

data.boxplot(column=['Fuel Consumption Hwy (L/100km)'],by='Fuel Type')
plt.show()

#d) Pomocu stupcastog dijagrama prikažite broj vozila po tipu goriva. Koristite metodu groupby.

fuelType = data.groupby('Fuel Type')['Make'].count()
fuelType.plot(kind='bar')
plt.show()

#e)Pomocu stup ´ castog grafa prikažite na istoj slici prosje ˇ cnu C02 emisiju vozila s obzirom na broj cilindara.

numOfCylinders = data.groupby(by='Cylinders')['CO2 Emissions (g/km)'].mean()
numOfCylinders.plot(kind='bar')
plt.ylabel('CO2 Emission')
plt.show()


