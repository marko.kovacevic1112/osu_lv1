import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import MinMaxScaler
import sklearn.linear_model as lm
from sklearn.metrics import (
    mean_absolute_error,
    mean_squared_error,
    r2_score
    )
import math
# For column : numpy_Array_name[  : ,column] 
# For row : numpy_Array_name[ row, :  ]


# U ovoj vježbi
# promatra se slucaj kada je izlazna veli ˇ cina ˇ y kontinuirana velicina odnosno vrijednosti vektora ˇ y su
# 32 Poglavlje 4. Linearni modeli za regresiju. Vrednovanje regresijskih modela.
# iz skupa realnih brojeva. Ovakvi problemi nazivaju se regresijski problemi.

data = pd.read_csv("data_C02_emission.csv")

inputVariables = data[['Engine Size (L)',
                   'Cylinders',
                   'Fuel Consumption City (L/100km)',
                   'Fuel Consumption Hwy (L/100km)',
                   'Fuel Consumption Comb (L/100km)',
                   'Fuel Consumption Comb (mpg)']]
X = inputVariables.to_numpy()
print(type(X))
y = data['CO2 Emissions (g/km)'].to_numpy()

X_train, X_test, y_train, y_test = train_test_split(X,y,train_size=0.8)

#b)
plt.scatter(X_train['Fuel Consumption City (L/100km)'],y_train, c='r', label='Train data')
plt.scatter(X_test['Fuel Consumption City (L/100km)'],y_test, c='b', label='Test data')
plt.legend()
plt.show()

#c) Standardizacija (skaliranje) ulaznih veličina
plt.figure()
plt.hist(X_train[:,1])

sc = MinMaxScaler()
X_train_n = sc.fit_transform(X_train)
X_test_n = sc.fit_transform(X_test)

plt.figure()
plt.hist(X_train_n[:,1])
plt.show()

#d)
linearModel = lm.LinearRegression()
linearModel.fit(X_train_n, y_train)
print(linearModel.coef_)
print(linearModel.intercept_)

#e)procjena izlazne velicine na temelju ulaznih velicina skupa za testiranje
y_test_p = linearModel.predict(X_test_n)
plt.scatter(y_test, y_test_p)
plt.plot((100,600),(100,600), c='r')
plt.xlabel('test')
plt.ylabel('predict')
plt.show()

#f)
MAE = mean_absolute_error(y_test, y_test_p)
MSE = mean_squared_error(y_test, y_test_p)
RMSE = math.sqrt(MSE)
R2 = r2_score(y_test, y_test_p)
print(f"MAE = {MAE}")
print(f"MSE = {MSE}")
print(f"RMSE = {RMSE}")
print(f"R2 = {R2}")
#g)
#kada sam maknuo dvije ulazne veličine evaluacijske metrike su postale bolje (manje su pogreške) no kada sam uklonio četiri ulazne veličine
#pogreške su veće nego u početku