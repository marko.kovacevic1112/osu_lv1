import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
import sklearn.linear_model as lm
from sklearn.preprocessing import OneHotEncoder
from sklearn.metrics import (
    mean_absolute_error,
    mean_absolute_percentage_error,
    mean_squared_error,
    r2_score
)
import math
#ako imam npr 3 kategorije tipa: dizel, benzin i elektricni vraca broj elemenata iz svake grupe
#data['Fuel Type'].value_counts()
#data['Fuel Type'].value_counts()['benzin'] => ovako mogu vidjeti koliko ima benzinskih auta




data = pd.read_csv("data_C02_emission.csv")

#potrebno ovo napraviti prije oneHotEncoding-a(pretvaranja kategorickog stupca u numericki)
data['Fuel Type'] = data['Fuel Type'].astype('category')

#dodjeljivanje numerickih vrijednosti u novi stupac
data['Fuel Type New'] = data['Fuel Type'].cat.codes

encoder = OneHotEncoder()

encodedData = pd.DataFrame(encoder.fit_transform(
    data[['Fuel Type New']]).toarray())

newData = data.join(encodedData)

X = newData[[
    'Engine Size (L)',
    'Cylinders',
    'Fuel Consumption City (L/100km)',
    'Fuel Consumption Hwy (L/100km)',
    'Fuel Consumption Comb (L/100km)',
    'Fuel Consumption Comb (mpg)',
    'Fuel Type New'
]].to_numpy()

y = newData['CO2 Emissions (g/km)'].to_numpy()

X_train, X_test, y_train, y_test = train_test_split(X,y,train_size=0.8)

model = lm.LinearRegression()
#treniranje modela
model.fit(X_train, y_train)

y_test_p = model.predict(X_test)


MAE = mean_absolute_error(y_test, y_test_p)
MSE = mean_squared_error(y_test, y_test_p)
RMSE = math.sqrt(MSE)
R2 = r2_score(y_test, y_test_p)
print(f"MAE = {MAE}")
print(f"MSE = {MSE}")
print(f"RMSE = {RMSE}")
print(f"R2 = {R2}")

maxMistakeInd = abs(y_test - y_test_p).argmax()
print(f"biggest mistake is on {data.iloc[maxMistakeInd]['Model']} model")


